using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarterPoint : MonoBehaviour
{
    public GameObject player;
    public GameObject checkPointSpawn;
    public GameObject initialSpawn;
    public bool respawn;
    Vector3 limit = new Vector3(0, -20, 0);
    Vector3 initalPoint;
    Vector3 chekPointPos;

    private void Awake()
    {
        //checkPointSpawn = GameObject.Find("CheckPointObj");
    }

    void Start()
    {
        
        player = GameObject.FindGameObjectWithTag("Player");
        initialSpawn = GameObject.FindGameObjectWithTag("InitialSPawn");

        initalPoint = initialSpawn.transform.position + new Vector3(0, 3, 0);

        
       

        player.transform.position = initalPoint;
    }

    private void Update()
    {
        if(checkPointSpawn != null)
            chekPointPos = checkPointSpawn.transform.position + new Vector3(0, 3, 0);

        if (player.transform.position.y <= limit.y)
        {

            respawn = true;
        }

        if(respawn == true)
        {
            this.transform.parent = null;
            if (checkPointSpawn == null)
                player.transform.position = initalPoint;
            else if (checkPointSpawn != null)
                player.transform.position = chekPointPos;
            respawn = false;
        }
    }
}
