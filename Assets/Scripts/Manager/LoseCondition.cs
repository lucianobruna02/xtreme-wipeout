using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseCondition : MonoBehaviour
{

    public GameObject player;
    public GameObject initialSpawn;
    //public GameObject checkPointSpawn;
    Vector3 initalPoint;
    //Vector3 checkPointPos;

    private void Awake()
    {
        //checkPointSpawn = GameObject.Find("CheckPointObj");
    }

    void Start()
    {
        
        player = GameObject.FindGameObjectWithTag("Player");
        initialSpawn = GameObject.FindGameObjectWithTag("InitialSPawn");
        //checkPointPos = checkPointSpawn.transform.position + new Vector3(0, 3, 0);
        //initalPoint = initialSpawn.transform.position + new Vector3(0, 3, 0);

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            initialSpawn.gameObject.GetComponent<StarterPoint>().respawn = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            initialSpawn.gameObject.GetComponent<StarterPoint>().respawn = true;
        }
    }
}
