using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalPoint : MonoBehaviour
{
    public int actualLevel;
    public int maxLevel;
    public GameObject Dev;
    public float pauseTimer = 30;
    public bool playerFinish = false;

    private void Awake()
    {
        //DontDestroyOnLoad(this.transform.gameObject);
        if (actualLevel == maxLevel)
            Dev.gameObject.SetActive(false);
        else
            Dev = null;
    }


    void Start()
    {
        
    }

    void Update()
    {
        if (Time.timeScale == 0)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                Application.Quit();
        }

        //if (playerFinish == true)
        //{
        //    pauseTimer -= Time.deltaTime;

        //    if(pauseTimer <= 0)
        //    {
        //        Time.timeScale = 0f;
        //    }
        //}
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (actualLevel < maxLevel)
            {
                NextLevel();
                SceneManager.LoadScene(actualLevel);
            }
               
            else if(actualLevel >= maxLevel)
            {
                Dev.gameObject.SetActive(true);
                playerFinish = true;
                Interlude.InterludeManager.KeyFound();
                Time.timeScale = 0f;
                //Cursor.lockState = CursorLockMode.Locked;
                //Cursor.visible = true;
            }
            
        }
        
    }

    void NextLevel()
    {
        actualLevel += 1;
        //SceneManager.LoadScene("Game");
    }
}
