using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrier : MonoBehaviour
{
    public Transform initialPos;
    public Transform finalPos;
    public Transform stayPos;
    public float speed;
    float step;
    public float timer;
    float privTimer;
    public bool alwaysInZone = false;
    bool move;
    public GameObject trigger;
    public bool pInZone;
    public bool moveToFinalPos = false;
    public bool moveToInitialPos = false;
    bool restTimer = false;

    public float parentTimer;
    public bool wrongFloor = false;
    float privParentTimer;
    public bool desParent = false;
    GameObject collisionObject;

    void Start()
    {
        if(stayPos != null)
        {
            wrongFloor = false;
            this.transform.position = stayPos.position;
        }
        else if(stayPos == null)
        {
            privParentTimer = parentTimer;
        }
        privTimer = timer;
    }

    void Update()
    {
        if(alwaysInZone == true)
        {
            pInZone = true;
        }
        step = speed * Time.deltaTime;
     
        if(alwaysInZone == false)
        {
            pInZone = trigger.GetComponent<BarrierTrigger>().playerIsInZone;
        }
        

        if (this.transform.position == initialPos.position)
        {
            moveToFinalPos = true;
        }

        if(stayPos == null)
        {
            if (this.transform.position == initialPos.position)
            {
                moveToInitialPos = false;
            }
        }

        if(moveToFinalPos == true)
        {
            this.transform.position = Vector3.MoveTowards(transform.position, finalPos.position, step);
        }
        if(moveToInitialPos == true)
        {
            this.transform.position = Vector3.MoveTowards(transform.position, initialPos.position, step);
        }

        if(this.transform.position == finalPos.position)
        {
            moveToFinalPos = false;
            if(stayPos != null)
            {
                this.transform.position = stayPos.position;
            }
            
        }

        if (stayPos == null && this.transform.position == finalPos.position)
        {
            moveToInitialPos = true;
        }

        if (stayPos != null)
        {
            if (this.transform.position == stayPos.position)
            {
                this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            }
            else
            {
                this.gameObject.GetComponent<MeshRenderer>().enabled = true;
            }
        }

        if(/*this.transform.position == stayPos.position*/pInZone == true)
        {
            restTimer = true;
        }

        if(restTimer == true)
        {
            privTimer -= Time.deltaTime;


            if (privTimer <= 0)
            {
                if(alwaysInZone == false)
                {
                    pInZone = false;
                }
                if(stayPos != null)
                {
                    this.transform.position = initialPos.position;
                }
                //else if(stayPos == null && this.transform.position == finalPos.position)
                //{
                //    moveToInitialPos = true;
                //}
                restTimer = false;
                privTimer = timer;
            }
        }


        if (desParent == true)
        {
            if(privParentTimer > 0)
            {
                privParentTimer -= Time.deltaTime;
            }
            

            if (privParentTimer <= 0)
            {
                collisionObject.transform.parent = null;
                desParent = false;
            }

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(stayPos == null)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                collisionObject = collision.gameObject;
                collision.transform.parent = this.transform;
            }
        }
        
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (stayPos == null)
            {
                desParent = true;

            }
        }
        else
        {
            desParent = false;
        }
    }
}
