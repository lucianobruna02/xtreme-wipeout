using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikesControl : MonoBehaviour
{

    public Animation anim;
    public bool showSpikes = false;
    public bool animExecute = false;
    public bool playerExit = false;

    public float waitTime;
    float interWaitTime;

    public float showSpike;
    float interShowSpike;

    public float hideSpike;
    float interHideSpike;

    int sS = 0;
    int hS = 0;

    void Start()
    {
        anim = this.gameObject.GetComponent<Animation>();
        anim.Play("Anim_TrapNeedle_Hide");
        showSpikes = false;
        playerExit = true;
        animExecute = true;
        interShowSpike = showSpike;
        interWaitTime = waitTime;
        interHideSpike = hideSpike;

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
            showSpikes = !showSpikes;

        SpikeState();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            showSpikes = true;
            playerExit = false;
        }
        
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerExit = true;
        }
   
    }

    void SpikeState()
    {
        if(showSpikes == true)
        {
            interShowSpike -= Time.deltaTime;
            if (interShowSpike <= 0)
            {
                if (sS <= 0)
                {
                    anim.Play("Anim_TrapNeedle_Show");
                    interHideSpike = hideSpike;
                    hS = 0;

                    sS++;
                }
                else if (sS >= 1)
                {
                    interWaitTime -= Time.deltaTime;

                    if (interWaitTime <= 0)
                    {
                        animExecute = true;
                    }
                }
            }
            
            
            //anim.Play("Anim_TrapNeedle_Idle");
        }

        if(animExecute == true && playerExit == true)
        {
            showSpikes = false;
            animExecute = false;
            playerExit = false;
        }

        if(showSpikes == false)
        {
            interHideSpike -= Time.deltaTime;

            if (interHideSpike <= 0 && playerExit == false && animExecute == false) 
            {
                if (hS <= 0)
                {
                    anim.Play("Anim_TrapNeedle_Hide");
                    interShowSpike = showSpike;
                    interWaitTime = waitTime;
                    animExecute = false;

                    sS = 0;
                    hS++;
                }
            }
           
           
            //anim.Play("Anim_TrapNeedle_Idle");
        }

    }

   
}
