using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingFloor2 : MonoBehaviour
{
    public bool floorIsFall = false;
    public bool floorIsUp = true;
    public bool decreaseTime = false;
    public Transform fallPos;
    public float speed;
    float step;
    bool desParent = false;
    public float parentTimer;
    float privParentTimer;
    public float timer;
    float privTimer;
    bool move;
    public bool playerIsInFloor;
    GameObject collisionObject;

    private void Start()
    {
        playerIsInFloor = false;
        privTimer = timer;
        privParentTimer = parentTimer;
    }

    void FixedUpdate()
    {
        step = speed * Time.deltaTime;



        if (playerIsInFloor == true)
        {
            decreaseTime = true;
        }


        if (decreaseTime == true)
        {
            if (this.gameObject.GetComponentInParent<Barrier>() != null)
            {
                if (this.gameObject.GetComponentInParent<Barrier>().wrongFloor == true)
                {
                    this.gameObject.GetComponentInParent<Barrier>().moveToFinalPos = false;
                    this.gameObject.GetComponentInParent<Barrier>().moveToInitialPos = false;
                }
               
            }
            

            privTimer -= Time.deltaTime;


            if (privTimer <= 0)
            {
                transform.position = Vector3.MoveTowards(transform.position, fallPos.position, step);

            }
            //if (this.transform.position == fallPos.position)
            //{
            //    privTimer = timer;
            //    decreaseTime = false;
            //    floorIsFall = true;
            //    playerIsInFloor = false;
            //}
        }
        //if (timer <= 0)
        //    decreaseTime = false;

        if(desParent == true)
        {
            privParentTimer -= Time.deltaTime;

            if (privParentTimer <= 0)
            {
                collisionObject.transform.parent = null;
                desParent = false;
            }

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collisionObject = collision.gameObject;
            playerIsInFloor = true;
            collision.transform.parent = this.transform;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerIsInFloor = false;

            desParent = true;

           
            //collision.transform.parent = null;
        }
    }
}
