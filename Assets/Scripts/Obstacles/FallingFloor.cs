using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingFloor : MonoBehaviour
{

    public bool floorIsFall = false;
    public bool floorIsUp = true;
    public bool decreaseTime = false;
    public Transform initialPos;
    public Transform fallPos;
    public float speed;
    float step;
    public float timer;
    float privTimer;
    bool move;
    public bool playerIsInFloor;
    bool isMoving;

    private void Start()
    {
        playerIsInFloor = false;
        isMoving = false;
        privTimer = timer;
    }

    void FixedUpdate()
    {
        step = speed * Time.deltaTime;

        if (this.transform.position == fallPos.position)
        {
            floorIsFall = true;
            floorIsUp = false;
        }
            
        else if (this.transform.position == initialPos.position)
        {
            floorIsUp = true;
            floorIsFall = false;
        }
            

        if(this.transform.position == initialPos.position || this.transform.position == fallPos.position)
        {
            isMoving = true;
        }
        else if(this.transform.position != initialPos.position || this.transform.position != fallPos.position)
        {
            isMoving = false;
        }

        if (playerIsInFloor == true && isMoving == true)
        {
            decreaseTime = true;
        }

        else if (playerIsInFloor == false)
        {

          if(floorIsUp == false && floorIsFall == true)
            {
                transform.position = Vector3.MoveTowards(transform.position, initialPos.position, step);
            }
                
          else if(floorIsUp == true && floorIsFall == false && this.transform.position != initialPos.position)
                transform.position = Vector3.MoveTowards(transform.position, fallPos.position, step);

            if (floorIsUp == true && floorIsFall == false && this.transform.position == initialPos.position && decreaseTime == false)
                privTimer = timer;
        }

        if(decreaseTime == true)
        {
            privTimer -= Time.deltaTime;


            if (privTimer <= 0)
            {
                transform.position = Vector3.MoveTowards(transform.position, fallPos.position, step);
                
            }
            if (this.transform.position == fallPos.position)
            {
                privTimer = timer;
                decreaseTime = false;
                floorIsFall = true;
                playerIsInFloor = false;
            }
        }
        //if (timer <= 0)
        //    decreaseTime = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerIsInFloor = true;
            collision.transform.parent = this.transform;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerIsInFloor = false;
            collision.transform.parent = null;
        }
    }
}
