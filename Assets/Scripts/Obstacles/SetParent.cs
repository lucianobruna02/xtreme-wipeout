using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetParent : MonoBehaviour
{
    public Rigidbody rb;
    Quaternion originalRotation;
    public float rotateSpeed;
    bool restoreRotation = false;

    bool startTimer = false;
    public float timer;
    bool timerDone = false;

    Vector3 rbVelocity;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rbVelocity = rb.angularVelocity;
        originalRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(startTimer == true)
        {
            timer -= Time.deltaTime;
        }

        if (timer <= 0) 
        {
            restoreRotation = true;
        }

        if (restoreRotation)
        {
            //transform.rotation = Quaternion.Lerp(transform.rotation, originalRotation, Time.time * rotateSpeed);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, 0f, 0f), 1.0f * Time.deltaTime);
            if (transform.rotation == originalRotation)
            {
                timer = 2f;
                startTimer = false;
                restoreRotation = false;
            }
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            rb.angularVelocity = rbVelocity;
            timer = 2f;
            //collision.gameObject.transform.parent = this.transform;
        }
    }
    //private void OnCollisionStay(Collision collision)
    //{
    //    if (collision.gameObject.CompareTag("Player"))
    //    {
    //        if (collision.gameObject.transform.rotation.x >= 45  || collision.gameObject.transform.rotation.z >= 45)
    //        {
    //            collision.gameObject.transform.parent = null;
    //        }
    //    }
    //}

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            rb.angularVelocity = Vector3.zero;
            startTimer = true;
            //collision.gameObject.transform.parent = null;
        }
    }
}
